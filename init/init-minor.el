;;; Minor modes
(use-package rx :straight t)

(use-package flycheck
  :straight t
  :disabled t
  :after (blackout)
  :config
  (global-flycheck-mode)
  (setq-default flycheck-indication-mode 'left-margin)
  (add-hook 'flycheck-mode-hook #'flycheck-set-indication-mode)
  ;; (setq flycheck-check-syntax-automatically '(save mode-enable))
  (blackout 'flycheck-mode)

  (add-to-list
   'display-buffer-alist
   `(,(rx bos "*Flycheck errors*" eos)
     (display-buffer-reuse-window display-buffer-in-side-window)
     (side . bottom)
     (reusable-frames . visible)
     (window-height . 0.20))))

;; https://github.com/emacsattic/isolate
(use-package isolate :straight t)

(use-package ox-reveal
 :straight t
 :config (setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js"))

(use-package htmlize :straight t)

(use-package rg
 :straight t
 :config
 (setq wgrep-auto-save-buffer t)
 ;; Muscle memory...
 (rg-define-search
  ag-project
  "Search like ag-project"
  :query ask
  :format literal
  :files "everything"
  :dir project)
 (rg-define-search
  ag-project-regex
  "Search like ag-project"
  :query ask
  :format regex
  :files "everything"
  :dir project))

(use-package smartparens
 :if nil
 :straight t
 :init (require 'smartparens-config)
 :config
 (smartparens-global-mode t)
 (show-smartparens-global-mode t)
 (setq sp-show-pair-from-inside t))

(use-package whitespace-cleanup-mode
  :straight t
  :after (blackout)
  :config
  (global-whitespace-cleanup-mode)
  (blackout 'whitespace-cleanup-mode))

(use-package projectile
 :straight t
 :after (blackout)
 :config (setq projectile-indexing-method 'alien)
 (setq projectile-enable-caching t)
 ;; Git changes aren't always picked up, so let the cache expire.
 (setq projectile-file-exists-local-cache-expire (* 5 60))
 (blackout 'projectile-mode))

(use-package counsel-projectile
 :straight t
 :config
 ;; Workaround for https://github.com/ericdanan/counsel-projectile/issues/179
 (setq counsel-projectile-switch-project 'counsel-projectile-switch-project-action-find-file)
 (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
 (counsel-projectile-mode))

(use-package elisp-autofmt :straight t)

(use-package highlight-indent-guides
 :straight t
 :if nil ;; very slow with Emacs 29
 :config
 ;; column method is not the prettiest, but causes less interference with other
 ;; packages.
 (setq highlight-indent-guides-method 'column)
 (setq highlight-indent-guides-responsive 'top)
 (add-hook 'prog-mode-hook 'highlight-indent-guides-mode))

(use-package indent-bars
  :straight t
  :config
   (setq
    indent-bars-color '(highlight :face-bg t :blend 0.75)
    indent-bars-color-by-depth '(:regexp "outline-\\([0-9]+\\)" :blend 1)
    indent-bars-unspecified-fg-color "white"
    indent-bars-unspecified-bg-color "black"
    ;; indent-bars-prefer-character t
    )
   :hook ((prog-mode) . indent-bars-mode))

;; note: init executes before loading package, config afterwards
(use-package diff-hl
  :straight t
  :config
  (setq-default diff-hl-side 'right)
  (diff-hl-margin-mode)
  (global-diff-hl-mode))

;; location depends on host
(defun lbn/determine-howm-directory()
  "Determine directory for HOWM notes based on host."
  (let ((hosts (list
                '("7afd086c957295f0ef274f6c27b10b6d9739a497" "~/shared_projects/howm/")
                '("7bb017a65f9e0c31223eb0faa3a24ba2aeec167e" "~/shared_projects/howm/")
                '("23f0e5369b429825068cc7e482161fc42a8f71a8" "~/notes/howm/")
                '("7aca103c9ab0728f9b8ceccb8e81147328f5cb33" "~/shared_projects/howm/")))
        (host (secure-hash 'sha1 lbn/hostname)))
    (cl-dolist (x hosts)
      (when (equal host (cl-first x))
        (message (format "Picked %s" (cl-second x)))
        (cl-return (cl-second x))))))

(use-package howm :straight t
  ;; Settings based on https://leahneukirchen.org/blog/archive/2022/03/note-taking-in-emacs-with-howm.html
  ;; Directory configuration
  :init
  (setq lbn/howm-home (lbn/determine-howm-directory))
  (setq howm-home-directory lbn/howm-home)
  (setq howm-directory lbn/howm-home)
  (setq howm-keyword-file (expand-file-name ".howm-keys" howm-home-directory))
  (setq howm-history-file (expand-file-name ".howm-history" howm-home-directory))
  (setq howm-file-name-format "%Y/%m/%Y-%m-%d-%H%M%S.md")
  (setq howm-view-title-header "#")

  ;; Rename buffers to their title
  (add-hook 'howm-mode-hook 'howm-mode-set-buffer-name)
  (add-hook 'after-save-hook 'howm-mode-set-buffer-name)

  ;; Use ripgrep as grep
  (setq howm-view-use-grep t)
  (setq howm-view-grep-command "rg")
  (setq howm-view-grep-option "-nH --no-heading --color never")
  (setq howm-view-grep-extended-option nil)
  (setq howm-view-grep-fixed-option "-F")
  (setq howm-view-grep-expr-option nil)
  (setq howm-view-grep-file-stdin-option nil))

(message "init-minor done")
