;;; Major modes

(use-package nftables-mode :straight t)

(use-package crystal-mode
  :straight t
  :config
  (add-to-list 'auto-mode-alist '("\\.cr$" . crystal-mode))
  (add-to-list 'interpreter-mode-alist '("crystal" . crystal-mode)))

(use-package protobuf-mode :straight t)

(use-package swift-mode :straight t)

(use-package gleam-ts-mode
  :straight t
  :mode (rx ".gleam" eos))

(use-package org
 :straight org-contrib
 :init
 ;; pre-load
 :config
 ;; post-load
 (org-babel-do-load-languages
  'org-babel-load-languages
  '((awk . t)
    (C . t)
    (emacs-lisp . t)
    (gnuplot . t)
    (js . t)
    (perl . t)
    (python . t)
    (shell . t))))

(use-package markdown-mode
 :straight t
 :if (not (string-equal system-type "windows-nt"))
 :commands (markdown-mode gfm-mode)
 :mode (("README\\.md\\'" . gfm-mode) ("\\.md\\'" . markdown-mode) ("\\.markdown\\'" . markdown-mode))
 :init (setq markdown-command "pandoc")
 :bind
 (:map
  markdown-mode-map
  ("M-." .
   (lambda ()
     (interactive)
     (progn
       (xref-push-marker-stack)
       (markdown-follow-thing-at-point nil))))))


(use-package go-mode
  :straight t
  :config
  (add-hook 'go-mode-hook (lambda () (setq tab-width 4))))

(use-package groovy-mode :straight t)

;; Should be disabled locally in projects
(use-package google-c-style
 :straight t
 :config
 (add-hook 'c-mode-common-hook 'google-set-c-style)
 (add-hook 'c-mode-common-hook 'google-make-newline-indent))

(use-package cmake-mode :straight t)

(use-package vterm
 :ensure t
 :straight t
 :custom (vterm-always-compile-module t))
(use-package multi-vterm :straight t :ensure t)

;;; Guess what? Slow on Windows
(use-package ox-jira :straight t :if (not (string-equal system-type "windows-nt")))

(use-package docker-compose-mode :straight t)
(use-package dockerfile-mode :straight t)

(use-package yaml-mode :straight t)

(use-package magit :straight t)


(defun lbrockna/toggle-eat-mode ()
  "Toggle between buffer and terminal modes."
  (interactive)
  (if eat--semi-char-mode
      (eat-emacs-mode)
    (eat-semi-char-mode)))

(use-package eat
 :straight
 (eat
  :type git
  :host codeberg
  :repo "akib/emacs-eat"
  :files
  ("*.el"
   ("term" "term/*.el")
   "*.texi"
   "*.ti"
   ("terminfo/e" "terminfo/e/*")
   ("terminfo/65" "terminfo/65/*")
   ("integration" "integration/*")
   (:exclude ".dir-locals.el" "*-tests.el")))
 :config (setq eat-term-name "eat-256color") (setq eat-enable-shell-prompt-annotation nil)
 :bind (:map eat-mode-map ("C-c C-t" . lbrockna/toggle-eat-mode)))

;; Treesitter
(use-package treesit
  :mode (("\\.tsx\\'" . tsx-ts-mode) ("\\.ts\\'" . typescript-ts-mode))
  :preface
  (defun mp-setup-install-grammars ()
    "Install Tree-sitter grammars if they are absent."
    (interactive)
    (dolist (grammar
             ;; Note the version numbers. These are the versions that
             ;; are known to work with Combobulate *and* Emacs.
             '((css . ("https://github.com/tree-sitter/tree-sitter-css" "v0.20.0"))
               (go . ("https://github.com/tree-sitter/tree-sitter-go" "v0.20.0"))
               (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript" "v0.20.1" "src"))
               (json . ("https://github.com/tree-sitter/tree-sitter-json" "v0.20.2"))
               (tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "tsx/src"))
               (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "typescript/src"))
               ))
      (add-to-list 'treesit-language-source-alist grammar)
      ;; Only install `grammar' if we don't already have it
      ;; installed. However, if you want to *update* a grammar then
      ;; this obviously prevents that from happening.
      (unless (treesit-language-available-p (car grammar))
        (treesit-install-language-grammar (car grammar)))))

  ;; Optional. Combobulate works in both xxxx-ts-modes and
  ;; non-ts-modes.

  ;; You can remap major modes with `major-mode-remap-alist'. Note
  ;; that this does *not* extend to hooks! Make sure you migrate them
  ;; also
  (dolist (mapping
           '(
             (css-mode . css-ts-mode)
             (typescript-mode . typescript-ts-mode)
             (js2-mode . js-ts-mode)
             (go-mode . go-ts-mode)
             (json-mode . json-ts-mode)
             (js-json-mode . json-ts-mode)))
    (add-to-list 'major-mode-remap-alist mapping))
  :config
  (mp-setup-install-grammars)
  ;; Do not forget to customize Combobulate to your liking:
  ;;
  ;;  M-x customize-group RET combobulate RET
  ;;
)

(straight-register-package 'typescript-mode)

(when (> emacs-major-version 28)
  (setq-default typescript-ts-mode-indent-offset 2))

(when (< emacs-major-version 29)
  (use-package typescript-mode :straight t)
  (setq-default typescript-indent-level 2))


(message "init-major done")
