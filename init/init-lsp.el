;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Language Servers (and related)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package eglot
  :config
  (add-to-list 'eglot-server-programs '((js-mode typescript-mode (typescript-ts-base-mode :language-id "typescript")) . (eglot-deno "deno" "lsp")))
  (add-to-list 'eglot-server-programs '((perl-mode cperl-mode) . ("~/.local/bin/perlnavigator")))

  (add-hook 'typescript-mode-hook 'eglot-ensure)
  (add-hook 'typescript-ts-mode-hook 'eglot-ensure)
  (add-hook 'c-mode-hook 'eglot-ensure)
  (add-hook 'csharp-mode-hook 'eglot-ensure)
  (add-hook 'c++-mode-hook 'eglot-ensure)
  (add-hook 'objc-mode-hook 'eglot-ensure)
  (add-hook 'go-mode-hook 'eglot-ensure)
  (add-hook 'swift-mode-hook 'eglot-ensure)
  (add-hook 'perl-mode-hook 'eglot-ensure)
  (add-hook 'cperl-mode-hook 'eglot-ensure)
  ;; https://github.com/joaotavora/eglot/discussions/1356
  ;; doesn't work...
  (setq-default eglot-prefer-plaintext t)

  (defclass eglot-deno (eglot-lsp-server) ()
    :documentation "A custom class for deno lsp.")

  (cl-defmethod eglot-initialization-options ((server eglot-deno))
    "Passes through required deno initialization options"
    (list :enable t
    :lint t))
)

(use-package lsp-mode
 :straight t
 :commands lsp
 :disabled t
 :custom
 (lsp-completion-provider :none) ;; we use Corfu!
 :init
 (defun my/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(flex))) ;; Configure flex
 :hook
   (lsp-completion-mode . my/lsp-mode-setup-completion)
 :config ;;(require 'lsp-clients)
 (add-hook 'typescript-mode-hook #'lsp) ;; for typescript support
 (add-hook 'c-mode-hook #'lsp)
 (add-hook 'csharp-mode-hook #'lsp)
 (add-hook 'c++-mode-hook #'lsp)
 (add-hook 'objc-mode-hook #'lsp)
 (add-hook 'go-mode-hook #'lsp)
 (add-hook 'swift-mode-hook #'lsp)
 (setq lsp-prefer-flymake nil)
 (setq read-process-output-max (* 512 1024)) ;; 1mb to improve performance
 (dolist (dir '("[/\\\\][.]build$"))
   (push dir lsp-file-watch-ignored))

 ;; Go specific
 ;; Set up before-save hooks to format buffer and add/delete imports.
 ;; Make sure you don't have other gofmt/goimports hooks enabled.
 (defun lsp-go-install-save-hooks ()
   (add-hook 'before-save-hook #'lsp-format-buffer t t)
   (add-hook 'before-save-hook #'lsp-organize-imports t t))
 (add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

 (lsp-register-custom-settings '(("gopls.experimentalWorkspaceModule" t t)))

 ;; workaround for flycheck errors in the headline (which frequently is not valid
 ;; English. See https://github.com/syl20bnr/spacemacs/issues/14292
 (add-hook 'lsp-headerline-breadcrumb-mode-hook 'flyspell-mode-off))

(use-package lsp-sourcekit
  :disabled t
 :straight t
 :after lsp-mode
 :if (string-equal system-type "darwin")
 :config
 (setq
  lsp-sourcekit-executable
  "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/sourcekit-lsp"))


(use-package lsp-ui
  :disabled t
 :straight t
 :config
 (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
 (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
 (setq
  lsp-ui-sideline-enable t
  lsp-ui-doc-enable nil
  lsp-ui-flycheck-enable t
  lsp-ui-imenu-enable nil
  lsp-ui-sideline-ignore-duplicate t
  lsp-ui-sideline-diagnostic-max-lines 3))


(use-package lsp-pyright
  :disabled t
 :straight t
 :hook
 (python-mode
  .
  (lambda ()
    (require 'lsp-pyright)
    (lsp)))) ; or lsp-deferred

(message "init-lsp done")
