;;; Config for rarely used projects

;; curl "https://chromium.googlesource.com/chromium/src/+/df175a8c3d1c1060f96d123a5896fdfbe4b3782d/tools/gn/misc/emacs/gn-mode.el?format=TEXT" | base64 -d > gn-mode.el
(load "gn-mode")

(use-package powershell :straight t)

(straight-use-package
 '(applescript-mode :type git :host github :repo "emacsorphanage/applescript-mode")
 :if (string-equal system-type "darwin"))


;; Still better than LSP + omnisharp-roslyn?
(use-package omnisharp
 :disabled t
 :straight t
 :after (company)
 :config
 (push 'company-omnisharp company-backends)
 (add-hook 'csharp-mode-hook 'omnisharp-mode)
 (add-hook 'csharp-mode-hook #'flycheck-mode))

(use-package csharp-mode :straight t :if (< emacs-major-version 29))

(use-package ansi-color
  :hook (compilation-filter . ansi-color-compilation-filter))

(message "init-misc done")
