#!/bin/sh
# Setup symlink from .emacs.d to this repository

set -eux

echo "Removing existing symlink"
rm -f "$HOME/.emacs.d"

echo "Linking to repo"
ln -s "$PWD" "$HOME/.emacs.d"
