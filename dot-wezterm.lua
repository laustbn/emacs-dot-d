local wezterm = require('wezterm')

local all_characters = [[`1234567890-=qwertyuiop[]\asdfghjkl:;'zxcvbnm,./<>_]]
local characters = {}

for i = 1, #all_characters do
  table.insert(characters, all_characters:sub(i, i))
end


local function mappings(maps)
  local result = {}
  local seen = {}
  local mod_key

  if string.find(wezterm.target_triple, "darwin") ~= nil then
    mod_key = 'CMD'
  else
    mod_key = 'ALT'
  end

  for _, mapping in ipairs(maps) do
    mapping.mods = mapping.mods and mapping.mods:gsub('MOD', mod_key)
    table.insert(result, mapping)

    seen[mapping.mods .. " " .. mapping.key:lower()] = true
  end

  if mod_key == 'CMD' then
    for _, key in ipairs(characters) do
      for _, mods in ipairs({ 'CMD', 'CMD|SHIFT','CTRL|CMD|SHIFT', 'CTRL|SHIFT' }) do
        local combo = mods .. ' ' .. key

        if not seen[combo] then
          seen[combo] = true

          table.insert(result, {
            key = key,
            mods = mods,
            action = wezterm.action.SendKey { key = key, mods = mods:gsub('CMD', 'ALT') },
          })
        end
      end
    end
  end

  return result
end

config = {}
if wezterm.config_builder then
  config = wezterm.config_builder()
end

config.disable_default_key_bindings = true
-- config.enable_csi_u_key_encoding = true
config.keys = mappings({})

return config
