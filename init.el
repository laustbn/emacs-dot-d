;;; early-init.el --- early bird  -*- no-byte-compile: t -*-
(setq load-prefer-newer t)

(defvar bootstrap-version)
(setq straight-repository-branch "develop")
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent
         'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


;; Install use-package via straight
(straight-use-package 'use-package)
(setq-default use-package-always-demand t)
;; Put custom-faces and other auto-saved options into this file. Then don't load
;; it! This prevents Emacs from messing with the per-platform options.
(setq custom-file (concat user-emacs-directory "/custom.el"))

(setq package-user-dir (expand-file-name "elpa" user-emacs-directory))

;; Local misc packages that cannot be installed from Melpa/etc
(add-to-list 'load-path (concat user-emacs-directory "/misc"))
(add-to-list 'load-path (concat user-emacs-directory "/init"))

;; Don't pop up Warnings buffer unless it's really bad
(setq warning-minimum-level :error)



(use-package s :straight t)

(defvar lbn/hostname (s-trim-right (shell-command-to-string "hostname")))

(use-package auto-compile
 :straight t
 :init
 (auto-compile-on-load-mode)
 (auto-compile-on-save-mode))


;; Don't fetch and build these projects to avoid issues with eglot (similar to
;; https://github.com/joaotavora/eglot/discussions/1436)
(straight-use-package '(project :type built-in))
(straight-use-package '(xref :type built-in))

(load "init-completion")
(load "init-major")
(load "init-minor")
(load "init-lsp")
(load "init-misc")

;;; Needed for flycheck
(add-hook 'c++-mode-hook 'set-fill-column 'display-fill-column-indicator)

(use-package modern-cpp-font-lock :straight t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package editorconfig :straight t :config (editorconfig-mode 1))

(straight-use-package '(docstr :type git :host github :repo "jcs-elpa/docstr"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Modeline

(use-package blackout
 :straight t
 :config
 (blackout 'smartparens-mode)
 (blackout 'editorconfig-mode)
 (blackout 'eldoc-mode)
 (blackout 'auto-revert-mode)
 ;; (blackout 'ivy-mode)
 (blackout 'flymake-mode)
 (blackout 'flyspell-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package free-keys :straight t)

;;; Not on Windows
(use-package exec-path-from-shell
 :straight t
 :if (not (string-equal system-type "windows-nt"))
 :config (exec-path-from-shell-initialize))

(use-package citre
 :straight t
 :config
 (if (string-equal system-type "darwin")
     (setq citre-readtags-program "/opt/local/bin/ureadtags")))


(use-package expand-region :straight t)

(use-package cmake-font-lock :straight t)


(use-package which-key :straight t :after (blackout) :config (which-key-mode +1) (blackout 'which-key-mode))

(use-package fireplace :straight t)

(use-package clang-format :straight t :config (global-set-key [C-M-tab] 'clang-format-region))

(use-package multiple-cursors :straight t)

(use-package smex :straight t)

(use-package nyan-mode
 :straight t
 :if (display-graphic-p)
 :config
 (setq nyan-bar-length 20)
 (setq nyan-wavy-trail t)
 (nyan-mode))

(use-package avy
 :straight t
 :config
 ;; (global-set-key (kbd "C-\\") 'avy-goto-char)
 (global-set-key (kbd "C-'") 'avy-goto-char-2)
 ;; (global-set-key (kbd "C-'") 'avy-goto-char-timer)
 (setq avy-background t))

;;; Enable if used
(use-package yasnippet :disabled t :straight t :config (yas-global-mode 1))

(use-package visual-fill-column :straight t)

(defun lbn/idle-highlight-hook ()
  "Enable idle highlight."
  (interactive)
  (idle-highlight-mode))

(use-package idle-highlight-mode
 :straight t
 :config (add-hook 'prog-mode-hook 'lbn/idle-highlight-hook))

(use-package hydra :straight t)

;; Enable richer annotations using the Marginalia package
(use-package marginalia
 :straight t
 ;; Either bind `marginalia-cycle` globally or only in the minibuffer
 :bind (("M-A" . marginalia-cycle) :map minibuffer-local-map ("M-A" . marginalia-cycle))

 ;; The :init configuration is always executed (Not lazy!)
 :init

 ;; Must be in the :init section of use-package such that the mode gets
 ;; enabled right away. Note that this forces loading the package.
 (marginalia-mode))

;;; Also slow on Windows
(use-package git-gutter
 :straight t
 :if (and nil (not (string-equal system-type "windows-nt")))
 :config
 ;; Show changes to git-tracked files automatically
 (global-git-gutter-mode +1))

(use-package ibuffer-projectile
 :straight t
 :config (global-set-key (kbd "C-x C-b") 'ibuffer)
 (add-hook
  'ibuffer-hook
  (lambda ()
    (ibuffer-projectile-set-filter-groups)
    (unless (eq ibuffer-sorting-mode 'alphabetic)
      (ibuffer-do-sort-by-alphabetic)))))


(use-package devdocs :straight t)

(use-package multiple-cursors
 :straight t
 :config
 (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
 (global-set-key (kbd "C->") 'mc/mark-next-like-this)
 (global-set-key (kbd "C-M->") 'mc/unmark-next-like-this)
 (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
 (global-set-key (kbd "C-M-<") 'mc/unmark-previous-like-this)
 (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
 (global-set-key (kbd "C-S->") 'mc/mark-more-like-this-extended))

(straight-use-package '(ztree-diff :type git :host codeberg :repo "fourier/ztree"))

;;(use-package ztree-diff :straight t)

(use-package esup
 :straight t
 ;; To use MELPA Stable use ":pin mepla-stable",
 ;; :pin melpa
 :config
 ;; Work around a bug where esup tries to step into the byte-compiled
 ;; version of `cl-lib', and fails horribly.
 (setq esup-depth 0)
 :commands (esup))


(use-package fold-this :straight t)

; probably better to check for existence of linum--mode in goto-with-feedback
(display-line-numbers-mode 0)

(global-set-key [remap goto-line] 'goto-line-with-feedback)
(defun goto-line-with-feedback ()
  "Show line numbers temporarily, while prompting for the line number input."
  (interactive)
  (let ((line-numbers-off-p (not display-line-numbers-mode)))
    (unwind-protect
        (progn
          (when line-numbers-off-p
            (display-line-numbers-mode 1))
          (call-interactively 'goto-line))
      (when line-numbers-off-p
        (display-line-numbers-mode -1)))))

;; Write backup files to own directory
(setq backup-directory-alist
      `(("." . ,(expand-file-name (concat user-emacs-directory "backups")))))


;;; Misc settings
(setq inhibit-startup-screen t)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)
(tool-bar-mode -1)
(show-paren-mode 1)
(column-number-mode 1)
(fset 'yes-or-no-p 'y-or-n-p)
(setq calendar-week-start-day 1)
(setq-default nxml-child-indent 2)
(c-set-offset (quote cpp-macro) 0 nil)
(global-unset-key [(control z)])
(pending-delete-mode t)
(setq-default mouse-yank-at-point t)

;; slow compared to perl mode
(defalias 'perl-mode 'cperl-mode)

(defun lbn/setup-cperl-hook()
(setq cperl-close-paren-offset (- cperl-indent-level)
      cperl-continued-statement-offset cperl-indent-level
      cperl-indent-parens-as-block t
      cperl-tab-always-indent t
      cperl-indent-wrt-brace nil))

(add-hook 'cperl-mode-hook 'lbn/setup-cperl-hook)
(add-hook 'prog-mode-hook #'hl-line-mode)
(add-hook 'text-mode-hook #'hl-line-mode)
(add-hook 'prog-mode-hook #'flymake-mode)


;; Hopefully fix colors inside GNU Screen
(add-to-list 'term-file-aliases '("screen.xterm-256color" . "xterm-256color"))

;; Show keystrokes in progress
(setq echo-keystrokes 0.1)

;; Controversial!
;;(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq-default indent-tabs-mode nil)
(setq tab-always-indent 'complete)

(add-hook 'org-mode-hook (lambda () (flyspell-mode 1)))

(setq-default fill-column 80)
(defun set-fill-column ()
  (setq fill-column 80))

;; Emacs via screen when using the "xterm256" TERM setting (for fixing other
;; issues) requires this
(global-set-key [select] 'end-of-line)

(use-package wucuo
  :straight t
  :after (blackout)
  :config
  (add-hook 'prog-mode-hook #'wucuo-start)
  (blackout 'wucuo-mode))

;; Enable spelling also in programming modes.
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'text-mode-hook (lambda () (setq show-trailing-whitespace t)))
(add-hook 'prog-mode-hook (lambda () (setq show-trailing-whitespace t)))

(global-set-key [M-left] 'windmove-left) ; move to left window
(global-set-key [M-right] 'windmove-right) ; move to right window
(global-set-key [M-up] 'windmove-up) ; move to upper window
(global-set-key [M-down] 'windmove-down) ; move to downer window


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Visual
(autoload 'zoom-in/out "zoom-frm" "Zoom current frame or buffer in or out" t)

(define-key ctl-x-map [(control ?+)] 'zoom-in/out)
(define-key ctl-x-map [(control ?-)] 'zoom-in/out)
(define-key ctl-x-map [(control ?=)] 'zoom-in/out)
(define-key ctl-x-map [(control ?0)] 'zoom-in/out)

(global-set-key
 (if (boundp 'mouse-wheel-down-event) ; Emacs 22+
     (vector (list 'control mouse-wheel-down-event))
   [C-mouse-wheel]) ; Emacs 20, 21
 'zoom-in)
(global-set-key
 (if (boundp 'mouse-wheel-down-event) ; Emacs 22+
     (vector (list 'control 'meta mouse-wheel-down-event))
   [C-M-mouse-wheel]) ; Emacs 20, 21
 'zoom-all-frames-in)
(when (boundp 'mouse-wheel-up-event) ; Emacs 22+
  (global-set-key (vector (list 'control mouse-wheel-up-event)) 'zoom-out)
  (global-set-key (vector (list 'control 'meta mouse-wheel-up-event)) 'zoom-all-frames-out))

(global-set-key [S-mouse-1] 'zoom-in)
(global-set-key [C-S-mouse-1] 'zoom-out)
;; Get rid of `mouse-set-font' or `mouse-appearance-menu':
(global-set-key [S-down-mouse-1] nil)


;; https://christiantietze.de/posts/2023/01/modus-themes-v4-changes/
(use-package modus-themes
  :straight t
  :if t
  :after (howm)
  :init (require 'modus-themes)
  ;; https://protesilaos.com/emacs/modus-themes#h:7ea8fa66-1cd8-47b0-92b4-9998a3068f85
  (defun my-modus-themes-custom-faces (&rest _)
    (modus-themes-with-colors
      (custom-set-faces
       `(action-lock-face ((,c :inherit button)))
       `(howm-mode-keyword-face (( )))
       `(howm-mode-ref-face ((,c :inherit link)))
       `(howm-mode-title-face ((,c :inherit modus-themes-heading-0)))
       `(howm-mode-wiki-face ((,c :inherit link)))
       `(howm-reminder-deadline-face ((,c :foreground ,date-deadline)))
       `(howm-reminder-late-deadline-face ((,c :inherit bold :foreground ,date-deadline)))
       `(howm-reminder-defer-face ((,c :foreground ,date-scheduled)))
       `(howm-reminder-scheduled-face ((,c :foreground ,date-scheduled)))
       `(howm-reminder-done-face ((,c :foreground ,prose-done)))
       `(howm-reminder-todo-face ((,c :foreground ,prose-todo)))
       `(howm-reminder-normal-face ((,c :foreground ,date-common)))
       `(howm-reminder-today-face ((,c :inherit bold :foreground ,date-common)))
       `(howm-reminder-tomorrow-face ((,c :inherit bold :foreground ,date-scheduled)))
       `(howm-simulate-todo-mode-line-face ((,c :inherit bold)))
       `(howm-view-empty-face (( )))
       `(howm-view-hilit-face ((,c :inherit match)))
       `(howm-view-name-face ((,c :inherit bold)))
       `(iigrep-counts-face1 ((,c :foreground ,rainbow-1)))
       `(iigrep-counts-face2 ((,c :foreground ,rainbow-2)))
       `(iigrep-counts-face3 ((,c :foreground ,rainbow-3)))
       `(iigrep-counts-face4 ((,c :foreground ,rainbow-4)))
       `(iigrep-counts-face5 ((,c :foreground ,rainbow-5))))))

  (add-hook 'enable-theme-functions #'my-modus-themes-custom-faces)

 ;; Add all your customizations prior to loading the themes
 (setq modus-themes-to-toggle '(modus-vivendi modus-operandi))
 (setq
  modus-themes-italic-constructs t
  modus-themes-bold-constructs t
  modus-themes-region '())

 ;; :config
 (load-theme (car modus-themes-to-toggle) t nil)
 ;; Load the theme files before enabling a theme (else you get an error).
 :bind ("<f5>" . modus-themes-toggle))


(use-package green-phosphor-theme
  :straight t
  :if nil
  :config
  (load-theme 'green-phosphor t))

(use-package moody
 :straight t
 :if nil
 :ensure t
 :config
 (setq x-underline-at-descent-line t)
 (moody-replace-mode-line-buffer-identification)
 (moody-replace-vc-mode)
 (moody-replace-eldoc-minibuffer-message-function))

(use-package page-break-lines :straight t)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Outline and folding

;; https://karthinks.com/software/simple-folding-with-hideshow/
(defun hs-cycle (&optional level)
  (interactive "p")
  (let (message-log-max
        (inhibit-message t))
    (if (= level 1)
        (pcase last-command
          ('hs-cycle (hs-hide-level 1) (setq this-command 'hs-cycle-children))
          ('hs-cycle-children
           ;; TODO: Fix this case. `hs-show-block' needs to be
           ;; called twice to open all folds of the parent
           ;; block.
           (save-excursion (hs-show-block)) (hs-show-block) (setq this-command 'hs-cycle-subtree))
          ('hs-cycle-subtree (hs-hide-block))
          (_
           (if (not (hs-already-hidden-p))
               (hs-hide-block)
             (hs-hide-level 1)
             (setq this-command 'hs-cycle-children))))
      (hs-hide-level level)
      (setq this-command 'hs-hide-level))))

(defun hs-global-cycle ()
  (interactive)
  (pcase last-command
    ('hs-global-cycle (save-excursion (hs-show-all)) (setq this-command 'hs-global-show))
    (_ (hs-hide-all))))


(add-hook
 'prog-mode-hook
 (lambda ()
   (unless (derived-mode-p 'gleam-ts-mode)
   (hs-minor-mode)
   (blackout 'hs-minor-mode)
   (local-set-key (kbd "C-<tab>") 'hs-cycle)
   (local-set-key (kbd "C-S-<tab>") 'hs-global-cycle))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Platform specific configuration


;; Clipetty for clipboard integration in terminal
(use-package clipetty
 :if (not (display-graphic-p))
 :straight t
 :ensure t
 :hook (after-init . global-clipetty-mode))

;; check OS type
(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows
  (progn
    (message "Microsoft Windows")
    (custom-set-faces
     '(default ((t (:family "JetBrains Mono"))))
     '(lsp-face-highlight-textual ((t (:background "brightblack")))))))

 ((string-equal system-type "darwin") ; Mac OS X
  (cond
   ((member "IBM Plex Mono" (font-family-list))
    (custom-set-faces
     '(default ((t (:family "IBM Plex Mono" :weight medium :height 120))))
     '(lsp-face-highlight-textual ((t (:background "brightblack"))))))
   ((member "JetBrains Mono" (font-family-list))
    (custom-set-faces
     '(default ((t (:family "JetBrains Mono" :weight light :height 120))))
     '(lsp-face-highlight-textual ((t (:background "brightblack"))))))
   ((member "Hack" (font-family-list))
    (custom-set-faces
     '(default ((t (:family "Hack"))))
     '(lsp-face-highlight-textual ((t (:background "brightblack"))))))
   ((member "Monaco" (font-family-list))
    (custom-set-faces
     '(default ((t (:family "Monaco"))))
     '(lsp-face-highlight-textual ((t (:background "brightblack")))))))

  (global-set-key (kbd "<home>") 'move-beginning-of-line)
  (global-set-key (kbd "<end>") 'move-end-of-line)
  (setq mac-command-modifier 'meta))

 ((string-equal system-type "gnu/linux") ; linux
  (cond
   ((member "IBM Plex Mono" (font-family-list))
    (set-frame-font "IBM Plex Mono-10" nil t))
   ((member "JetBrains Mono" (font-family-list))
    (custom-set-faces
     '(default ((t (:family "JetBrains Mono"))))
     '(lsp-face-highlight-textual ((t (:background "brightblack"))))))
   ((member "Ubuntu Mono" (font-family-list))
    (custom-set-faces
     '(default ((t (:family "Ubuntu Mono"))))
     '(lsp-face-highlight-textual ((t (:background "brightblack"))))))
   )))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/
(if (string-equal system-type "darwin")
    ;; Work around a bug or misconfiguration in LSP where it fails to find
    ;; Xcoxe's clangd (it would seem to be a bug as the full path appears in LSP's log output...)
    (setq
     exec-path
     (append
      exec-path
      '("/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/"))))

;; Not enabled by default as it clashes with lsp
(use-package topsy
 :straight (topsy :type git :host github :repo "alphapapa/topsy.el")
 :after lsp-mode)

(with-eval-after-load 'semantic
  (cl-delete-if
   (lambda (x) (string-match-p "^semantic-" (symbol-name x))) completion-at-point-functions))

;; TODO: support converting relative filenames to absolute ones
(defun xx/copy-file-path ()
  "Copy project relative buffer filepath to kill ring.
If buffer is not backed by a file, copy buffer name.  If file is
not in Projectile project, copy entire path."
  (interactive)
  (let ((path
         (if (buffer-file-name)
             (file-relative-name buffer-file-name (projectile-project-root))
           (buffer-name))))
    (kill-new path)
    (message "Copied: %s" path)))

(defun xx/copy-function-name ()
  "Copy the current function name to the clipboard using 'which-function-mode'."
  (interactive)
  (let ((name
         (gethash
                   (selected-window)
                   which-func-table)))
    (kill-new name)
    (message "Copied: %s" name)))



;; Found somewhere on the internet
(defun xx/arrayify (start end quote)
  "Turn strings on newlines into a QUOTEd, comma-separated one-liner."
  (interactive "r\nMQuote: ")
  (let ((insertion
         (mapconcat
          (lambda (x) (format "%s%s%s" quote x quote))
          (split-string (buffer-substring start end)) ", ")))
    (delete-region start end)
    (insert insertion)))

(put 'narrow-to-region 'disabled nil)

(add-to-list 'safe-local-variable-values '(eval sh-set-shell "zsh"))
(add-to-list 'safe-local-variable-values '(eval ignore-errors (whitespace-cleanup-mode)))

(which-function-mode 1)
(setq-default column-number-mode t)
(setq-default groovy-indent-offset 2)
(setq-default js-indent-level 2)
(setq-default nxml-attribute-indent 4)
(setq-default sgml-basic-offset 4)
(setq-default nxml-child-indent 4)
(setq-default default-tab-width 4)
(setq-default nxml-attribute-indent 4)
(setq-default sgml-basic-offset 4)
(setq-default nxml-child-indent 4)
(setq-default c-basic-offset 4)
(delete-selection-mode 1)

;; Random things from https://github.com/jamescherti/minimal-emacs.d
(setq scroll-preserve-screen-position t)
(setq fast-but-imprecise-scrolling t)

(setq show-paren-delay 0.1
      show-paren-highlight-openparen t
      show-paren-when-point-inside-paren t
      show-paren-when-point-in-periphery t)

(setq minibuffer-prompt-properties
      '(read-only t intangible t cursor-intangible t face
                  minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
(setq-default make-window-start-visible t)


;; Use proper PATH on remotes
(add-to-list 'tramp-remote-path 'tramp-own-remote-path)


(if (fboundp 'scroll-bar-mode)
    (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode)
    (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode)
    (menu-bar-mode -1))

(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)


(defun my/disable-scroll-bars (frame)
  (modify-frame-parameters frame '((vertical-scroll-bars . nil) (horizontal-scroll-bars . nil))))
(add-hook 'after-make-frame-functions 'my/disable-scroll-bars)

(setq-default sh-basic-offset 2)
(setq-default sh-indentation 2)
(setq-default show-paren-mode t)
(setq-default tool-bar-mode nil)
(setq-default compilation-scroll-output 'first-error)
(setq-default mouse-avoidance-mode 'exile)
(setq-default
 tab-width 4
 help-window-select t)

(setq confirm-kill-emacs 'y-or-n-p)

;; (defun my/org-inline-css-hook (exporter)
;;   "Insert custom inline css to automatically set the background of code to whatever theme I'm using's background."
;;   (when (eq exporter 'html)
;;     (let* ((my-pre-bg (face-background 'default))
;;            (my-pre-fg (face-foreground 'default)))
;;       (setq
;;        org-html-head-extra
;;        (concat
;;         org-html-head-extra
;;         (format "<style type=\"text/css\">\n pre.src {background-color: %s; color: %s;}</style>\n"
;;                 my-pre-bg my-pre-fg))))))
;; (add-hook 'org-export-before-processing-hook 'my/org-inline-css-hook)

;; https://emacs-china.org/t/org/9479/5
(defun chunyang-yank-html ()
  "Yank HTML from clipboard as Org or Markdown code."
  (interactive)
  (let* ((result
          (condition-case err
              ;; hex-encoded string:
              ;;           < m e t a ......>
              ;; «data HTML3C6D657461......3E»
              (do-applescript "the clipboard as «class HTML»")
            (error
             ;; assume it's user's fault
             (user-error "Can't get HTML data from the clipboard: %s"
                         (error-message-string err)))))
         (data (substring result 10 -1))
         (html
          (with-temp-buffer
            (set-buffer-multibyte nil)
            (let* ((i 0))
              (while (> (length data) (+ 2 i))
                (insert (string-to-number (substring data i (+ 2 i)) 16))
                (cl-incf i 2)))
            (decode-coding-region (point-min) (point-max) 'utf-8 t)))
         (target
          (cond
           ((derived-mode-p 'org-mode)
            "org")
           ;; the official Markdown doesn't support table?
           (t
            "gfm"))))
    (insert
     (with-temp-buffer
       (if (zerop
            (call-process-region html nil "pandoc"
                                 nil t nil
                                 ;; https://stackoverflow.com/a/35812743/2999892
                                 "-f" "html-native_divs-native_spans" "-t" target))
           (buffer-string)
         (error "pandoc failed: %s" (buffer-string)))))))


(defun bsc-eval-after-load-xterm ()
  ;; Modified from https://gist.github.com/gnachman/b4fb1e643e7e82a546bc9f86f30360e4
  (unless (display-graphic-p)
    ;; Take advantage of iterm2's CSI u support (https://gitlab.com/gnachman/iterm2/-/issues/8382).
    (xterm--init-modify-other-keys)
    (when (and (boundp 'xterm-extra-capabilities) (boundp 'xterm-function-map))
      (message "Setting up terminal")
      ;; fix M-S-ret for org mode
      (define-key xterm-function-map "\e\[13;4u" [(control meta shift ?\r)])
      (define-key xterm-function-map "\e\[27;4;13~" [(control meta shift ?\r)])
      (let ((c 32))
        (while (<= c 126)
          (mapc
           (lambda (x)
             ;; define-key can take a vector that wraps a list of
             ;; events, e.g. [(control shift ?a)] for C-S-a
             (define-key
              xterm-function-map (format (car x) c) (vector (append (cdr x) (cons c '())))))
           '( ;; with ?.VT100.formatOtherKeys: 0
             ("\e\[27;3;%d~" meta)
             ("\e\[27;4;%d~" meta shift)
             ("\e\[27;5;%d~" control)
             ("\e\[27;6;%d~" control shift)
             ("\e\[27;7;%d~" control meta)
             ("\e\[27;8;%d~" control meta shift)
             ;; with ?.VT100.formatOtherKeys: 1
             ("\e\[%d;3u" meta)
             ("\e\[%d;4u" meta shift)
             ("\e\[%d;5u" control)
             ("\e\[%d;6u" control shift)
             ("\e\[%d;7u" control meta)
             ("\e\[%d;8u" control meta shift)))
          (setq c (1+ c)))))))
(eval-after-load "xterm" '(bsc-eval-after-load-xterm))


;; Old habits die hard
(defun fci-mode ()
  "Turn on display fill column mode."
  (interactive)
  (display-fill-column-indicator-mode 'toggle))

;; Configure clipboard integration
(defun socket-clipboard-invoke ()
  "Invoke socket-clipboard."
  (let ((contents (shell-command-to-string "socat UNIX-CONNECT:$HOME/.cache/clipboard.sock -")))
    (cond
     ((string= contents "")
      nil)
     ((string= contents (car kill-ring))
      nil)
     (t
      contents))))

(if (file-exists-p (concat (getenv "HOME") "/.cache/clipboard.sock"))
    (setq interprogram-paste-function 'socket-clipboard-invoke))

(global-set-key
 (kbd "C-c C-y")
 (lambda ()
   (interactive)
   (insert (shell-command-to-string "socat UNIX-CONNECT:$HOME/.cache/clipboard.sock -"))))

 (defun sort-symbols (reverse beg end)
      "Sort symbols in region alphabetically, in REVERSE if negative.
    See `sort-words'."
      (interactive "*P\nr")
      (sort-regexp-fields reverse "\\(\\sw\\|\\s_\\)+" "\\&" beg end))

(provide 'init)
;;; init.el ends here

;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
